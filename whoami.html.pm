#lang pollen
◊define-meta[title]{whoami}

Let's learn more about the terminal by asking it ◊em{about ourselves}.

In your terminal, type:
◊call{whoami}

You should see:
◊response{garthbrooksfan69}

(Or, whatever is your username on this computer.)

It's good to know where you stand with your computer.  It has a concept of 'you', but doesn't really know ◊em{you}.  Everything it knows is based on how you've represented yourself in text.

◊spirit{whoami}
