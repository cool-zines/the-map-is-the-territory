#lang pollen

◊define-meta[title]{Introduction}

Beneath the visual surface of your computer is an old and powerful magic, a silent but quick stream of energy that the computer draws from for power.  This magic is hidden but always present, like the sacred well held in the base of a cathedral.

This hidden place has many names: Shell, Terminal, Bash, Zsh, The Command Line. All of these names are correct, but incomplete; accurate to a part, but unable to describe the whole. Like all magical things, there are aspects of the command line always beyond our articulation.

Too, Like so many magical things, the secular world will always try to deform and defang it.  Modern tech culture will describe the command line as an obscure productivity tool; something you learn only to impress other tech folk (a meaningless activity) or to become a "power user" (a meaningless phrase).  Conventional tech wisdom will tell you that the command line is an intimidating, obscure, imposing place -- impossible to learn and dangerous to use.  This is only an attempt to hide its true nature:  the command line is a place made entirely of our first occult technology, the word.

The command line is pure language, and to exist in it is to practice all the reality-shifting and world manifesting power of metaphor and dialogue.  This is a place of empowerment, tangible creativity, and mystic bewilderment. While it can be dangerous, it's also exceedingly helfpul if you know how to listen.

This place is waiting for you, but will not make itself known until you ask for it.  You must take the first step.

I love the terminal, the shell, bash, the command line.  I discovered it during a time of extreme digital burnout and this discovery was honestly one of the most important parts of my life. Because you are cool, and likely my friend (or future friend!), I want to share the command line with you.

This zine is an introduction to interacting with your computer through the terminal, and a hint towards all the beautiful radical power possible there.  We will learn some basics by doing spellwork, and by the end you will have literally gathered magical fragments from the ether and cast a spell from a modest digital altar.  At the same time, this zine will teach the same terms and abilities you'd find in a technical guide and you can use these skills to do practical, business-like things (this does not detract from the magic inherent in the commands -- the tarot can be great for card games, and magical herbs work great in sauces and soaps).

Hopefully I'm able to share the wonder and delight of the terminal and for it to be meaningful for you.  Let me know if so, and keep it to yrself if not!
