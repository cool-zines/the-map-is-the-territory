#lang pollen

◊define-meta[title]{preparation}

To get the most out of this zine you will need a computer, a quiet space, and about an hour without distraction.

On the computer you will need a terminal, as we'll be typing commands directly into it.  The way you get to your terminal depends on what type of computer you're using.  Click on the bright square next to your OS to get more deets:
mac◊fn["mac"], linux◊fn["linux"], or windows◊fn["windows"].

(And whenever you see this type of link, click on it to get more deeeeeeets!)

I recommend reading this zine in one sitting.  Put on some nice music, brew a tea, have a good time!

◊|footnotes|

◊fndef["mac"]{Luckily, the terminal is already installed, just slightly hidden away. You can find it with spotlight by clicking the magnifying icon in your menubar (or typing ◊code{cmd+space}) then searching for terminal.  Alternately, you can find it in ◊code{applications > utilities > terminal}.}

◊fndef["linux"]{Linux loves the terminal. No matter what 'distro' you are using, the terminal will likely be prominent.  On Ubuntu, for example, you can find it by clicking the programs bar and searching terminal.}

◊fndef["windows"]{Windows can be a bit difficult, and to be honest, I don't know enough to be helpful.  If you are on windows 10, you can install linux within your windows computer.  You can read a guide about how to install Ubuntu(a popular distribution of linux) here: ◊a[#:href "https://ubuntu.com/tutorials/tutorial-ubuntu-on-windows#1-overview"]{Ubuntu's tutorial to install on windows 10}

If you are not on 10, I've heard good things about the program 'git bash'.  Here is an ◊a[#:href "https://www.stanleyulili.com/git/how-to-install-git-bash-on-windows/"]{in depth tutorial on installing git bash}}
