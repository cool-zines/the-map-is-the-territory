#lang pollen

◊define-meta[title]{Make an Altar}

We've chatted and moved with our computer, now let's ◊em{create}.

While in your home directory◊fn[1], type:
◊call{mkdir altar}.
Like cd, mkdir does its thing quietly.  You won't see any confirmation, just a flash of the command line prompt.

However, when you type:
◊call{ls}
you will see altar listed among your home folders:
◊response{altar Desktop Downloads Pictures...}

◊spirit{mkdir}

Let's move into the altar:
◊call{cd altar}

Then confirm we are there:
◊call{pwd}
We should see something like:
◊response{/Users/You/altar}

Let's list everything here:
◊call{ls}
This gives...nothing. We've manifested ourselves an empty space.

Next, we'll fill it with blessings and spells.


◊|footnotes|

◊fndef[1]{You can double check you are here with ◊code{pwd}, you should see ◊code{/Users/$YOU} where $YOU is whatever username you've chosen on this computer.  Another way is to just type ◊code{cd ~}}



