#lang pollen

◊define-meta[title]{Now Where Could My Pipe Be?}

A sigil similar to ◊cmd{>>} is the powerful ◊cmd{|}, also called a ◊em{pipe}.  Pipes let you direct the output of one command as an input of another.  Said another way, they let you invoke multiple spirits, each transforming the stream of energy as it is offered to them.

For example, let's invoke the spirits of ◊cmd{history}.

◊spirit{history}

Let's say we wanted to see every word we've echoed during this session.
Type:
◊call{history}.

You will see a long list of commands, including something like:
◊response{57 echo "the internet is animist" >> blessing}
◊response{58 cat blessing}
◊response{59 mkdir .spells}
◊response{60 ls}
◊response{61 ls -a}
◊response{62 ls -a ~}
◊response{63 uname -a}


History lists every command we've entered this session, including our echo commands.  Now we could scroll through this list, mentally noting our echo commands, but this would be arduous and doesn't match our intent.

Instead, we can give this list to the spirit ◊code{grep}.

◊spirit{grep}

Using our pipe, we can invoke history and then pass its offering to our invocation of grep and get exactly what we wanted.

in your terminal, type:
◊call{history | grep echo}

Now you'll see:
◊response{ 18  echo "hardcoooooooore"}
◊response{ 48  echo "the internet is animist" >> blessing}
◊response{ 50  echo "you are very cool" >> blessing}

With pipes you can compose increasingly complex invocations, to be more expressive in this adopted bash tongue.

And, as we'll see next, the stream of energy we pass through these pipes does not need to come from just your computer.  You can pull information from across the world into your invocations too.
