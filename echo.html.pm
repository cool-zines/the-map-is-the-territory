#lang pollen

◊define-meta[title]{Echo}


The command line is a pleasantly quiet place.  Honestly, this was the first appeal of it to me... that I could escape the noise of modern tech while still using my computer.

We can play with this quietness with the command ◊cmd{echo}

In your terminal type:
◊call{echo "hardcooooore"}

This should return:
◊response{"hardcooooore"}

Echo echoes. We are in a place so massive and quiet we can hear(= read) our own voice.

◊spirit{echo}



