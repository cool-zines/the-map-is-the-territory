#lang pollen

◊define-meta[title]{Open The Terminal}


As you look at the terminal you should see...mostly nothing.  This is as it should be.  There are no notifications, no pop-up with daily tips, no indications of how you should start.

◊(figure "tmitt_terminal.png" #:alt "a pixeled rendition of the terminal, showing a mostly blank computer window with the text 'twonky:~zach$' in the top left corner")

So, to start: do nothing.  Notice how the terminal, too, does nothing.  No prompt will appear asking if you need a little more time, no chat window with a cheery reminder of commands to try.  The terminal just waits.  It has no expectations, no motives. It has no emotion at all.  It does not wait impatiently, nor patiently --  It simply waits◊fn[1].

The bit that keeps the terminal from being a full on blank page is a line of cryptic text and ◊code{$}.  The exact line will be different for each terminal.  You may see your name and a ◊code{~}, and perhaps the name of your computer.  You may see something like ◊code{bash 3.2$}.

This is what's known as the prompt, and it's where you can enter in text for your computer to respond.  It is the "line" part of ◊em{command line}.

To use the terminal is to engage in a dialogue with your computer.  You will ask it to do something and it will do it.  Most of the time it gives no outward response, just moves quickly and diligently through the task and waits silently for your next ask. If you ask a question of it, it will print its answer.  If it doesn't understand you or encountered an issue, it will print out its problem as best as it can articulate.

In all cases, when you see the prompt, when you see a cursor ready to type answers, you can know the dialogue is happening, that the computer is waiting for the next thing you type.

Let's go ahead and start this dialogue.

◊|footnotes|

◊fndef[1]{this description of the terminal is inspired by Tech Learning Collective, and their incredible command line foundations course}
