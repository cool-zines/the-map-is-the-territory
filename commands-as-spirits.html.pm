#lang pollen

◊define-meta[title]{Commands as Spirits}

I keep referring to words like ◊cmd{cd}, ◊cmd{ls}, and ◊cmd{pwd} as ◊em{commands}, but this word doesn't fully describe them.  Each of them is not just a word, but a ◊em{thing}—— it has its own home location in your computer, its own history and authors. It's a program that expects certain things when invoked and, if given them, will do what is asked.

But just describing them as tiny, composable programs isn't right either.  This doesn't capture the cultural lineage, the memory sense when you see them, the identity they hold in your mind as you type them, their inscrutable nature as you get to know them, nor the wonder in how they exist at all.

To me, a better name for them is spirits: a set of symbols that grow in aliveness each time we call upon them. You can give these spirits offerings to influence what they give back, but each spirit likes different things, and works best when you give them what they like.

For example, ◊cmd{ls} and ◊cmd{cd} both love file paths (e.g.  ◊code{Users/Zach/Manifestos/family-circus-is-psy-ops.txt}).  If you give them an existing file path, they'll list its contents, or move to it, respectively.  If you don't give them one, they'll just list or move to wherever you're currently located.

By knowing what these spirits want, you can build more specific and powerful invocations. For a small example, type:
◊call{ls ~}
◊code{~} means 'home directory' and so is a valid file path.  ◊cmd{ls} takes it and does what it was meant to do: give back the file contents of ◊code{~}.  Try ◊cmd{ls ~/Documents} and ◊cmd{ls ~/Pictures}.

Similarly, in the command line you don't have to move around by clicking through folders one by one. You can cd any file path and it'll take you right there.  If you give it no file path, then it just takes you home.

Let's go home now. Type:
◊call{cd ~}
