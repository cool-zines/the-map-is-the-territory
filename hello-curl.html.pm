#lang pollen

◊define-meta[title]{Hello Curl}

The system in your computer started out in a giant mainframe meant to be used by many people at once, and developed alongside the internet.  And, as much as a computer ◊em{can} want, your computer wants to connect to things outside itself and be connected to.  In the source code of the command line is the memories of a community mainframe, of a info-rich spore in a network of spores.

There's a host of spirits intended to stretch this world of text beyond just your computer.  One of my favourites is ◊cmd{curl}.  Curl will bring data from another location to your computer to join that stream of energy, ready to be directed and transformed as you please.

◊spirit{curl}

We can curl the data from websites as easily as ◊code{curl} + ◊code{website address}.

In your terminal, type:
◊call{curl -L https://solarpunk.cool}

You'll be greeted with the source code that makes up our home page, including all the comments that don't appear on a browser screen◊fn[1].

You can do this with any site.  Try it with ◊ext-link["https://www.youtube.com/watch?v=pk4mpEv2KPE"]{google.com} and see the mess of javascript hidden behind their minimal search bar.  Try it with ◊ext-link["http://johnclilly.com/"]{johnclilly.com} to see how sites used to be made.◊fn[2] 

There are some sites that are designed for the terminal first, a whole underground of webpages.  My favorite is ◊ext-link["https://wttr.in"]{wttr.in}.

Type:
◊call{curl wttr.in}

You will see a beautiful printout of the weather in your city.  Wanna see the current weather in my beautiful hometown of Tumwater, Washington◊fn[3]? Type: ◊call{curl wttr.in/tumwater}

For the hardcore weather report, try:
◊call{curl v2.wttr.in}.
And for the beautiful moon, try:
◊call{curl wttr.in/Moon}

We will use curl (and everything else) for our final spellwork.

◊|footnotes|

◊fndef[1]{the ◊cmd{-L} is so we follow redirects to get to the true site. in this case, redirecting from http to https://}
◊fndef[2]{and to learn ◊em{the truth}.}

◊fndef[3]{You can of course type in your hometown instead.  I didn't know what it was and didn't want to embarass myself in front of you.}
