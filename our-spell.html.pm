#lang pollen

◊define-meta[title]{Our Spell}

There is a spell hidden in four directions, across four servers, that can be gathered and cast with the right command line invocation.

It is a spell of confidence and empowerment, that I learned from ◊ext-link["https://www.yeyeluisahteish.com"]{Yeye Luisa Tesh} in her book Jambalaya◊fn[1].  It is one of the first spells I learned, so holds a special place in my heart, and I am happy to get to share it with you.

We are going to gather the fragments into our ◊code{.spells} folder, then invoke the spell by casting them at once in the right order and reading it aloud (preferably looking into a mirror◊fn[2]).

You can do this spell whenever you need, by running through these invocations again.

Let's get ready by moving into our .spells folder...

type:
◊call{cd ~/altar/.spells}

◊|footnotes|

◊fndef[1]{Yeye Luisah Tesh is a Yoruba high priestess, storyteller, author, dancer, cosmically cool being.  The book I recommend is ◊book{Jambalaya: The Natural Woman's Book of Personal Charms and Practical Rituals}.  It is this immensely readable mix of memoir and manual and for me was legit life-changing.  It's available ◊ext-link["https://www.amazon.com/Jambalaya-Natural-Personal-Practical-Rituals/dp/0062508598/ref=asap_bc?ie=UTF8"]{on Amazon} and likely at your ◊ext-link["https://www.lastwordbooks.org/"]{favorite local book store.}}

◊fndef[2]{not a black mirror}
